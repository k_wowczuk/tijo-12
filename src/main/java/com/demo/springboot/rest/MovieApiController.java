package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.domain.dto.UpdateMovie;
import com.demo.springboot.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MovieApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);
    private final MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {
        LOGGER.info("--- get movies");
        return movieService.openFile();
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    @ResponseBody
    @CrossOrigin
    public ResponseEntity updateMovie(@RequestBody UpdateMovie updateMovie, @PathVariable(required = false) String id) {
        return movieService.updateMovieInFileCsv(updateMovie, id);
    }

}