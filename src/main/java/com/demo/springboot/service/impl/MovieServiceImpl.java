package com.demo.springboot.service.impl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.domain.dto.UpdateMovie;
import com.demo.springboot.exception.IdException;
import com.demo.springboot.rest.MovieApiController;
import com.demo.springboot.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class MovieServiceImpl implements MovieService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Override
    public MovieListDto openFile() {
        List<MovieDto> listMovie = createListMovie();
        return new MovieListDto(listMovie);
    }

    @Override
    public ResponseEntity updateMovieInFileCsv(UpdateMovie updateMovie, String id) {
        List<MovieDto> movieDtos = createListMovie();
        try {
            movieDtos.stream()
                    .filter(line -> line.getMovieId() == Integer.parseInt(id))
                    .peek(line -> {
                        line.setTitle(updateMovie.getTitle());
                        line.setImage(updateMovie.getImage());
                        line.setYear(updateMovie.getYear());
                    }).findFirst().orElseThrow(() -> new IdException("ID not exist"));

            FileWriter writer = new FileWriter("C:\\Users\\48733\\Desktop\\TAIU\\movies\\movies.csv", false);
            StringBuilder sb = new StringBuilder();
            sb.append("ID;TITLE;YEAR;IMAGE;")
                    .append("\n");

            movieDtos.forEach(mt -> sb
                    .append(mt.getMovieId())
                    .append(';').append(mt.getTitle())
                    .append(';')
                    .append(mt.getYear())
                    .append(';')
                    .append(mt.getImage())
                    .append(';')
                    .append("\n")
            );

            writer.append(sb);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IdException e) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        movieDtos.clear();
        return new ResponseEntity(HttpStatus.OK);
    }

    private List<MovieDto> createListMovie() {
        List<MovieDto> movieDtos = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get("C:\\Users\\48733\\Desktop\\TAIU\\movies\\movies.csv"))) {
            stream.filter(line -> !line.trim().split(";")[0].equals("ID"))
                    .forEach(line -> movieDtos.add(new MovieDto(
                            Integer.parseInt(line.split(";")[0].trim()),
                            line.split(";")[1].trim(),
                            Integer.parseInt(line.split(";")[2].trim()),
                            line.split(";")[3].trim())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movieDtos;
    }
}


